# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved
import os
import sys
import math
import json
import shutil
import configargparse
from sklearn.model_selection import ShuffleSplit

import numpy as np
import torch
from torch.utils.data import DataLoader, DistributedSampler, random_split

import imgaug as ia
import imgaug.augmenters as iaa

from pytorch_lightning.core.lightning import LightningModule
from pytorch_lightning import Trainer, seed_everything

import data
from data import build_dataset, get_coco_api_from_dataset
from models.detr import DETR, SetCriterion, PostProcess
from models.backbone import build_backbone
from models.transformer import build_transformer
from models.matcher import build_matcher
import util.misc as utils
import util.box_ops as box_ops
from util.mAP import calculate_mAP


class LitDETR(LightningModule):

    def __init__(self, hparams, *args, **kwargs):
        super().__init__()

        self.hparams = hparams

        backbone = build_backbone(self.hparams)
        transformer = build_transformer(self.hparams)

        self.model = DETR(
            backbone,
            transformer,
            num_classes=self.hparams.num_classes,
            num_queries=self.hparams.num_queries,
            aux_loss=self.hparams.aux_loss,
        )

        if self.hparams.resume:
            self.resume()

        matcher = build_matcher(self.hparams)
        weight_dict = {'loss_ce': 1, 'loss_bbox': self.hparams.bbox_loss_coef}
        weight_dict['loss_giou'] = self.hparams.giou_loss_coef
        # TODO this is a hack
        if self.hparams.aux_loss:
            aux_weight_dict = {}
            for i in range(self.hparams.dec_layers - 1):
                aux_weight_dict.update({k + f'_{i}': v for k, v in weight_dict.items()})
            weight_dict.update(aux_weight_dict)

        losses = ['labels', 'boxes', 'cardinality']
        self.criterion = SetCriterion(self.hparams.num_classes, matcher=matcher,
                                weight_dict=weight_dict,
                                eos_coef=self.hparams.eos_coef, losses=losses)
        self.postprocessors = {'bbox': PostProcess()}

        # temporary path to save auxiliar mAP files
        self.temp_files_path = '.temp_files'
        if not os.path.exists(self.temp_files_path): # if it doesn't exist already
            os.makedirs(self.temp_files_path)

    def forward(self, x):
        return self.model(x)

    def prepare_data(self):
        # List image files from sroie_path
        image_files = np.array( [f for f in os.listdir(self.hparams.dataset_path) if f.endswith('.jpg') \
                and os.path.exists(os.path.join(self.hparams.dataset_path, f.replace('jpg', 'txt'))) ])
        image_files.sort()

        strat_split = ShuffleSplit(n_splits=1, test_size=0.2, random_state=42)
        (train_index, test_index) = next(strat_split.split(image_files))

        # data augmentation
        ia_tfms = iaa.SomeOf(2, [
            iaa.Affine(
                scale={"x": (0.85, 1.15), "y": (0.85, 1.15)},
                translate_percent={"x": (-0.05, 0.05), "y": (-0.05, 0.05)},
                rotate=(-5, 5),
                cval=255,
            ),
            iaa.AdditiveGaussianNoise(scale=(0., 0.1*255)),
            iaa.SigmoidContrast(gain=(3, 10), cutoff=(0.4, 0.6))
        ])

        self.dataset_train = build_dataset(image_files[train_index], image_set='train', args=self.hparams, ia_tfms=ia_tfms)
        self.dataset_val = build_dataset(image_files[test_index], image_set='val', args=self.hparams)

    def configure_optimizers(self):
        param_dicts = [
            {"params": [p for n, p in self.model.named_parameters() if "backbone" not in n and p.requires_grad]},
            {
                "params": [p for n, p in self.model.named_parameters() if "backbone" in n and p.requires_grad],
                "lr": self.hparams.lr_backbone,
            },
        ]
        optimizer = torch.optim.AdamW(param_dicts, lr=self.hparams.lr,
                                    weight_decay=self.hparams.weight_decay)
        lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, self.hparams.lr_drop)

        return [optimizer], [lr_scheduler]

    def train_dataloader(self):
        data_loader_train = DataLoader(self.dataset_train, self.hparams.batch_size, shuffle=True,
                                       collate_fn=utils.collate_fn, num_workers=self.hparams.num_workers)
        return data_loader_train

    def val_dataloader(self):
        data_loader_val = DataLoader(self.dataset_val, self.hparams.batch_size, shuffle=False,
                                     drop_last=False, collate_fn=utils.collate_fn, num_workers=self.hparams.num_workers)
        return data_loader_val

    def test_dataloader(self):
        data_loader_test = DataLoader(self.dataset_val, self.hparams.batch_size, shuffle=False,
                                     drop_last=False, collate_fn=utils.collate_fn, num_workers=self.hparams.num_workers)
        return data_loader_test

    def training_step(self, batch, batch_idx):
        samples, targets = batch
        outputs = self.model(samples)
        loss_dict = self.criterion(outputs, targets)
        weight_dict = self.criterion.weight_dict
        losses = sum(loss_dict[k] * weight_dict[k] for k in loss_dict.keys() if k in weight_dict)

        loss_dict_unscaled = {f'{k}_unscaled': v
                for k, v in loss_dict.items()}
        loss_dict_scaled = {k: v * weight_dict[k]
                for k, v in loss_dict.items() if k in weight_dict}
        losses_scaled = sum(loss_dict_scaled.values())

        if not math.isfinite(losses_scaled.item()):
            print("Loss is {}, stopping training".format(losses_scaled.item()))
            print(loss_dict)
            sys.exit(1)

        # add logging
        logs = {'loss': losses_scaled, 'class_error': loss_dict['class_error']}
        for k, v in loss_dict_scaled.items():
            if not self.is_auxiliar_loss(k):
                logs[k] = v
        for k, v in loss_dict_unscaled.items():
            if not self.is_auxiliar_loss(k):
                logs[k] = v

        return {'loss': losses, 'log_step': logs}

    def validation_step(self, batch, batch_idx):
        samples, targets = batch

        outputs = self.model(samples)
        loss_dict = self.criterion(outputs, targets)
        weight_dict = self.criterion.weight_dict

        loss_dict_scaled = {k: v * weight_dict[k]
                for k, v in loss_dict.items() if k in weight_dict}
        loss_dict_unscaled = {f'{k}_unscaled': v
                for k, v in loss_dict.items()}
        losses_scaled = sum(loss_dict_scaled.values())

        # add logging
        logs = {'val_loss': losses_scaled, 'val_class_error': loss_dict['class_error']}
        for k, v in loss_dict_scaled.items():
            if not self.is_auxiliar_loss(k):
                logs['val_' + k] = v
        for k, v in loss_dict_unscaled.items():
            if not self.is_auxiliar_loss(k):
                logs['val_' + k] = v

        # Compute the results
        orig_target_sizes = torch.stack([t["orig_size"] for t in targets], dim=0)
        results = self.postprocessors['bbox'](outputs, orig_target_sizes)
        res = {target['image_id'].item(): output for target, output in zip(targets, results)}

        # Preparing data to compute mAP
        self.save_temp_files(targets, res)

        return {'val_loss': losses_scaled, 'log': logs}

    def test_step(self, batch, batch_idx):
        samples, targets = batch

        outputs = self.model(samples)
        loss_dict = self.criterion(outputs, targets)
        weight_dict = self.criterion.weight_dict

        loss_dict_scaled = {k: v * weight_dict[k]
                for k, v in loss_dict.items() if k in weight_dict}
        loss_dict_unscaled = {f'{k}_unscaled': v
                for k, v in loss_dict.items()}
        losses_scaled = sum(loss_dict_scaled.values())

        # add logging
        logs = {'test_loss': losses_scaled, 'test_class_error': loss_dict['class_error']}
        for k, v in loss_dict_scaled.items():
            if not self.is_auxiliar_loss(k):
                logs['test_' + k] = v
        for k, v in loss_dict_unscaled.items():
            if not self.is_auxiliar_loss(k):
                logs['test_' + k] = v

        # Compute the results
        orig_target_sizes = torch.stack([t["orig_size"] for t in targets], dim=0)
        results = self.postprocessors['bbox'](outputs, orig_target_sizes)
        res = {target['image_id'].item(): output for target, output in zip(targets, results)}

        # Preparing data to compute mAP
        self.save_temp_files(targets, res)

        return {'test_loss': losses_scaled, 'log': logs}

    def training_epoch_end(self, outputs):
        avg_logs = {}
        for k in outputs[0]['log_step'].keys():
            avg_logs[k] = torch.stack([x['log_step'][k] for x in outputs]).mean()

        results = {
                'loss': avg_logs['loss'],
                'log': avg_logs,
                'progress_bar' : {'train_loss': avg_logs['loss']}
                }
        return results

    def validation_epoch_end(self, outputs):
        avg_logs = {}
        for k in outputs[0]['log'].keys():
            avg_logs[k] = torch.stack([x['log'][k] for x in outputs]).mean()

        # Compute mAP
        map_value = calculate_mAP(os.path.join(self.temp_files_path, 'preds'),
                                  os.path.join(self.temp_files_path, 'targets'),
                                  os.path.join(self.temp_files_path, 'output'),
                                  self.temp_files_path, True)
        shutil.rmtree(self.temp_files_path)
        os.makedirs(self.temp_files_path)

        avg_logs['mAP'] = torch.tensor(map_value)

        results = {
                'val_loss': avg_logs['val_loss'],
                'log': avg_logs,
                'mAP': map_value,
                'progress_bar' : {'mAP': torch.tensor(map_value)}
                # 'progress_bar' : {'val_loss': avg_logs['val_loss']}
                }
        return results

    def test_epoch_end(self, outputs):
        avg_logs = {}
        for k in outputs[0]['log'].keys():
            avg_logs[k] = torch.stack([x['log'][k] for x in outputs]).mean()

        # Compute mAP
        map_value = calculate_mAP(os.path.join(self.temp_files_path, 'preds'),
                                  os.path.join(self.temp_files_path, 'targets'),
                                  os.path.join(self.temp_files_path, 'output'),
                                  self.temp_files_path, True)
        shutil.rmtree(self.temp_files_path)
        os.makedirs(self.temp_files_path)

        avg_logs['mAP'] = torch.tensor(map_value)

        # write a metric file
        data = {}
        data['mAP'] = map_value
        data['test_loss'] = avg_logs['test_loss'].item()
        data['test_cardinality_error_unscaled'] = avg_logs['test_cardinality_error_unscaled'].item()
        with open('metric.json', 'w') as f:
            json.dump(data, f)

        results = {
                'test_loss': avg_logs['test_loss'],
                'log': avg_logs,
                'mAP': map_value,
                'progress_bar' : {'mAP': torch.tensor(map_value)}
                # 'progress_bar' : {'test_loss': avg_logs['test_loss']}
                }
        return results

    def resume(self):
        if self.hparams.resume.startswith('https'):
            checkpoint = torch.hub.load_state_dict_from_url(
                self.hparams.resume, map_location='cpu', check_hash=True)
        else:
            checkpoint = torch.load(self.hparams.resume, map_location='cpu')

        try:
            # self.model.load_state_dict(checkpoint['model'])
            # RAMON: temporary to load checkpoint from other experiment
            print(checkpoint.keys())
            state_dict = {}
            for (k, v) in checkpoint['state_dict'].items():
                if k.startswith('criterion'): continue
                if k[:6] == 'model.':
                    state_dict[k[6:]] = v
                else: state_dict[k] = v
            self.model.load_state_dict(state_dict)
        except:
            # ramon: this is a hack to load the parameters with a different number of classes
            new_state_dict = {}
            num_classes = self.hparams.num_classes + 1 # 'text' class + 'no-object' class
            for (k, v) in checkpoint['model'].items():
                if k == 'class_embed.weight':
                    v = v[-num_classes:, :]
                if k == 'class_embed.bias':
                    v = v[-num_classes:] # get the last two classes
                new_state_dict[k] = v
            self.model.load_state_dict(new_state_dict)
        print(f'Loading parameters from {self.hparams.resume}.')

    def is_auxiliar_loss(self, s):
        return any(i.isdigit() for i in s)

    def save_temp_files(self, targets, results):
        """Save temporary files for computing mAP"""
        for target in targets:
            # Get targets
            boxes_gt = target['boxes']
            labels_gt = target['labels']
            image_id = target['image_id'].item()
            orig_size = target['orig_size']

            # convert to [x0, y0, x1, y1] format and scale from relative [0, 1] to absolute [0, height] coordinates
            boxes_gt = box_ops.box_cxcywh_to_xyxy(boxes_gt)
            img_h, img_w = orig_size.unbind(0)
            scale_fct = torch.stack([img_w, img_h, img_w, img_h], dim=0)
            boxes_gt = boxes_gt * scale_fct

            # Get predictions
            scores_pred = results[image_id]['scores']
            labels_pred = results[image_id]['labels']
            boxes_pred = results[image_id]['boxes'] # already converted by the posprocessor

            # TODO: move this to another place
            if not os.path.exists(self.temp_files_path + '/targets'): # if it doesn't exist already
                os.makedirs(self.temp_files_path + '/targets')
                os.makedirs(self.temp_files_path + '/preds')
                os.makedirs(self.temp_files_path + '/output')

            # Saving the targets
            with open(os.path.join(self.temp_files_path, 'targets', str(image_id) + '.txt'), 'w') as f:
                for label, box in zip(labels_gt, boxes_gt):
                    box = ' '.join([ str(b) for b in box.cpu().numpy() ])
                    s = str(label.item()) + ' ' + box + '\n'
                    f.write(s)

            # Saving the predictions
            with open(os.path.join(self.temp_files_path, 'preds', str(image_id) + '.txt'), 'w') as f:
                for label, score, box in zip(labels_pred, scores_pred, boxes_pred):
                    score = score.item()
                    label = label.item()

                    # Keep the ones not classified as no-object with score >= 0.7
                    if score >= 0.7 and label < self.hparams.num_classes:
                        box = ' '.join([ str(b) for b in box.cpu().numpy() ])
                        s = str(label) + ' ' + str(score) + ' ' + box + '\n'
                        f.write(s)

    @staticmethod
    def add_model_specific_args(parent_parser):
        parser = configargparse.ArgumentParser(parents=[parent_parser], add_help=False)
        parser.add_argument('--num_classes', type=int, default=1,
                            help="Number of classes of the current dataset")

        # Model parameters
        # * Backbone
        parser.add_argument('--backbone', default='resnet50', type=str,
                            help="Name of the convolutional backbone to use")
        parser.add_argument('--dilation', action='store_true',
                            help="If true, we replace stride with dilation in the last convolutional block (DC5)")
        parser.add_argument('--position_embedding', default='sine', type=str, choices=('sine', 'learned'),
                            help="Type of positional embedding to use on top of the image features")

        # * Transformer
        parser.add_argument('--enc_layers', default=6, type=int,
                            help="Number of encoding layers in the transformer")
        parser.add_argument('--dec_layers', default=6, type=int,
                            help="Number of decoding layers in the transformer")
        parser.add_argument('--dim_feedforward', default=2048, type=int,
                            help="Intermediate size of the feedforward layers in the transformer blocks")
        parser.add_argument('--hidden_dim', default=256, type=int,
                            help="Size of the embeddings (dimension of the transformer)")
        parser.add_argument('--dropout', default=0.1, type=float,
                            help="Dropout applied in the transformer")
        parser.add_argument('--nheads', default=8, type=int,
                            help="Number of attention heads inside the transformer's attentions")
        parser.add_argument('--num_queries', default=100, type=int,
                            help="Number of query slots")
        parser.add_argument('--pre_norm', action='store_true')

        # * Segmentation
        parser.add_argument('--masks', action='store_true',
                           help="Train segmentation head if the flag is provided (not included for pytorch-lightning)")

        # Loss
        parser.add_argument('--no_aux_loss', dest='aux_loss', action='store_false',
                            help="Disables auxiliary decoding losses (loss at each layer)")
        # * Matcher
        parser.add_argument('--set_cost_class', default=1, type=float,
                            help="Class coefficient in the matching cost")
        parser.add_argument('--set_cost_bbox', default=5, type=float,
                            help="L1 box coefficient in the matching cost")
        parser.add_argument('--set_cost_giou', default=2, type=float,
                            help="giou box coefficient in the matching cost")
        # * Loss coefficients
        parser.add_argument('--mask_loss_coef', default=1, type=float)
        parser.add_argument('--dice_loss_coef', default=1, type=float)
        parser.add_argument('--bbox_loss_coef', default=5, type=float)
        parser.add_argument('--giou_loss_coef', default=2, type=float)
        parser.add_argument('--eos_coef', default=0.1, type=float,
                            help="Relative classification weight of the no-object class")

        return parser

def main():
    parser = configargparse.ArgParser('DETR training and evaluation script', config_file_parser_class=configargparse.YAMLConfigFileParser)
    parser.add('-c', '--my-config', required=True, is_config_file=True, help='config file path')
    parser.add_argument('--lr', default=1e-4, type=float)
    parser.add_argument('--lr_backbone', default=1e-5, type=float)
    parser.add_argument('--batch_size', default=2, type=int)
    parser.add_argument('--weight_decay', default=1e-4, type=float)
    parser.add_argument('--lr_drop', default=200, type=int)

    # dataset parameters
    parser.add_argument('--dataset_file', default='sroie')
    parser.add_argument('--dataset_path', type=str)
    parser.add_argument('--remove_difficult', action='store_true')

    parser.add_argument('--seed', default=42, type=int)
    parser.add_argument('--resume', default='', help='resume from checkpoint')
    parser.add_argument('--num_workers', default=2, type=int)

    # add all the available trainer options to argparse
    # ie: now --gpus --num_nodes ... --fast_dev_run all work in the cli
    parser = Trainer.add_argparse_args(parser)

    # add model specific args
    parser = LitDETR.add_model_specific_args(parser)
    args = parser.parse_args()

    print("git:\n  {}\n".format(utils.get_sha()))

    # setting the seed for reproducibility
    seed_everything(args.seed)

    # Defining the model
    model = LitDETR(args)

    # Defining the Trainer
    trainer = Trainer.from_argparse_args(args)
    trainer.fit(model)

    trainer.test()


if __name__ == '__main__':
    main()
