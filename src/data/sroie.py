import os
import numpy as np
from PIL import Image
from pathlib import Path

import torch
from torch.utils.data import Dataset
import torchvision as tv

import imgaug as ia
import imgaug.augmenters as iaa

from util import box_ops

class SROIEDataset(Dataset):
    def __init__(self, image_files, folderpath, height, width, scale,
                 ia_tfms=None,
                 mean=torch.tensor([0.485, 0.456, 0.406]),
                 std=torch.tensor([0.229, 0.224, 0.225])):
        self.image_files = image_files
        self.folderpath = folderpath
        self.height = height
        self.width = width
        self.scale = scale
        self.mean = mean
        self.std = std
        self.ia_tfms = ia_tfms

        self.tfms = tv.transforms.Compose([
            tv.transforms.ToTensor(),
            tv.transforms.Normalize(mean=self.mean, std=self.std)
        ])

    def __len__(self,):
        return len(self.image_files)

    def __getitem__(self, idx):
        image_path = os.path.join(self.folderpath, self.image_files[idx])
        image, bboxes = get_image_and_bboxes(image_path)

        size = (self.height, self.width)
        #h, w, _ = image.shape

        image, bboxes = get_input_image_and_bboxes(
            image, bboxes, image_size=size, scale=self.scale)

        if self.ia_tfms is not None:
            image, bboxes = self.ia_tfms(
              image=image, bounding_boxes=bboxes)
        bboxes = bboxes.clip_out_of_image()

        bboxes = torch.tensor(bboxes.to_xyxy_array(np.float32))
        bboxes = box_ops.box_xyxy_to_cxcywh(bboxes)
        bboxes = bboxes / torch.tensor([self.width, self.height, self.width, self.height], dtype=torch.float32)

        target = {}
        target['boxes'] = bboxes
        target['labels'] = torch.zeros(bboxes.shape[0], dtype=torch.int64)

        target["image_id"] = torch.as_tensor(idx)
        target["orig_size"] = torch.as_tensor([self.height, self.width])
        #target["orig_size"] = torch.as_tensor([int(h), int(w)])

        return self.tfms(image), target

def get_image(image_path):
    pil_image = Image.open(image_path).convert('RGB')
    np_image = np.array(pil_image)
    return np_image

def get_bboxes(annotations_path, shape):
    txt_annotations = open(annotations_path).read().split('\n')
    boxes = []
    for row in txt_annotations[:-1]:
        x0, y0, _, _, x2, y2, _, _, _ = row.split(',', 8)
        boxes.append(ia.BoundingBox(int(x0), int(y0), int(x2), int(y2)))
    boxes_on_image = ia.BoundingBoxesOnImage(boxes, shape=shape)
    return boxes_on_image

def get_image_and_bboxes(image_path):
    annotations_path = image_path.replace('.jpg', '.txt')
    image = get_image(image_path)
    bboxes = get_bboxes(annotations_path, shape=image.shape)
    return image, bboxes

def resizer(image=None, bboxes=None, size=(512, 512)):
    kwargs = {}
    if image is not None:
        kwargs['image'] = image
    if bboxes is not None:
        kwargs['bounding_boxes'] = bboxes
    assert len(kwargs), 'Needs image or bboxes to resize'
    return iaa.Resize({'width': size[1], 'height': size[0]})(**kwargs)

def get_input_image_and_bboxes(image, bboxes, image_size, scale):
    h, w = image_size
    mh, mw = h // scale, w // scale
    image = resizer(image=image, size=(h, w))
    bboxes = resizer(bboxes=bboxes, size=(mh, mw))
    return image, bboxes

def build(image_files, image_set, args, ia_tfms=None):
    root = Path(args.dataset_path)
    assert root.exists(), f'provided SROIE path {root} does not exist'

    dataset = SROIEDataset(image_files, args.dataset_path, height=960, width=480, scale=1, ia_tfms=ia_tfms)
    return dataset
