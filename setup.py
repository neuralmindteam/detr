import os
from distutils.core import setup
from setuptools import find_packages
pkg_dir = os.path.dirname(__name__)
with open(os.path.join(pkg_dir, 'requirements-dev.txt')) as fd:
    requirements = [req.strip() for req in fd.read().splitlines()
                    if not req.strip().startswith('#')]
# Extra dependencies that cannot be included only in install_requires
# such as private repositories
requirements += [
    # 'nbsvm @ git+ssh://git@bitbucket.org/neuralmindteam/nbsvm.git@v0.2.1#egg=nbsvm',
]
setup(
    name='detr',
    version='0.1.0',
    packages=find_packages('.'),
    long_description=open('README.md').read(),
    install_requires=requirements,
)
